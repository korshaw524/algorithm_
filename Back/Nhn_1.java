import java.util.Arrays;
import java.util.Scanner;

public class Nhn_1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = Integer.parseInt(sc.next());
		String[] s = new String[n];
		int[] cnt = new int[n];
		boolean[] chk = new boolean[n];
		int maxidx = -1;
		for (int i = 0; i < s.length; i++) {
			s[i] = sc.next();
			if (!chk[i]) {
				cnt[i]++;
				chk[i] = true;
				for (int j = 0; j < s.length; j++) {
					if (s[i].equals(s[j])) {
						cnt[i]++;
						chk[j] = true;
					}
				}
			}
			if (maxidx < cnt[i]) {
				maxidx = cnt[i];
				
			}
		}
		Arrays.sort(s);
		System.out.println(Arrays.toString(s));
		System.out.println(Arrays.toString(cnt));

	}

}
