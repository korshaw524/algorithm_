

import java.util.Scanner;

public class Main1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();// 시험장 갯수
		int[] room = new int[N];
		for (int i = 0; i < N; i++) {
			room[i] = sc.nextInt();// 각 시험장에 있는 응시자 수
		}
		int B = sc.nextInt();// 총감독관이 감시할 수 있는 사람수
		int C = sc.nextInt();// 부감독관이 감시할 수 있는 사람수

		long t_count = 0;
		for (int i = 0; i < N; i++) {
			if (room[i] <= B) {
				t_count++;
			}
			{
				int ans = (room[i] - B) / C;
				if ((room[i] - B) % C != 0) {
					ans++;
				}
				ans++;
				t_count += ans;
			}
		}
		System.out.println(t_count);
	}
}
