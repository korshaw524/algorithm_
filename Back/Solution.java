import java.util.Scanner;
//큐에 넣고 돌리기
public class Solution {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int tc = 1; tc <= T; tc++) {
			int n = sc.nextInt(); // 배열의 크기
			int[][] arr = new int[n][n];
			boolean[][] flag = new boolean[n][n];
			for (int i = 0; i < n; i++) { // 배열 받기
				for (int j = 0; j < n; j++) {
					int input = sc.nextInt();
					arr[i][j] = input;
					flag[i][j] = false;
				}
			}
			int center = n / 2;
			int sum = 0;
			int[] di = { -1, 1, 0, 0 };// 상, 하
			int[] dj = { 0, 0, -1, 1 };// 좌 ,우
			int[][] sv = new int[n][n];
			
			
			
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					
					
					if(i==center&&j==center) {
						flag[center][center]=true;
						for(int d =0;d<di.length;d++) {
							int ni = i+di[d];
							int nj = j+dj[d];
							flag[ni][nj] = true;
							sum+=arr[ni][nj];
							
						}
					}
				}
			}

		}
	}

}
