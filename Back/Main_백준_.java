import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main_백준_ {

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int m = sc.nextInt();
		LinkedList<Character> list = new LinkedList<>();
		int[][] arr = new int [n+1][m+1];
		for (int i = 0; i < n; i++) {
			String s = sc.next();
			for (int j = 0; j < m; j++) {
				list.add(s.charAt(j));
				char cc = list.pop();
				int a = cc - '0';
				arr[i+1][j+1]= a;
			}
			System.out.println(Arrays.toString(arr));
		}

		
	}

}
