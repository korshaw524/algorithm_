import java.util.*;

class pair1 {
	int x;
	int y;

	pair1(int x, int y) {
		this.x = x;
		this.y = y;
	}
}

public class Main_2178 {

	public static Queue<pair1> que;
	public static int V;
	public static int E, cnt;
	public static boolean[][] chk;
	public static char[][] vmap;
	static int[][] map, map2;
	static int[] di = { -1, 1, 0, 0 };
	static int[] dj = { 0, 0, -1, 1 };

	public static void bfs(int nx, int ny) {
		// 시작은 0,0 끝은 V-1,E-1
		
		que.add(new pair1(nx, ny));
		if (!que.isEmpty()) {
			pair1 p = que.remove();
			int x = p.x;
			int y = p.y;
			chk[x][y] = true;
			for (int a = 0; a < 4; a++) {
				int ni = x + di[a];
				int nj = y + dj[a];
				if (ni >= 0 && nj >= 0 && ni < V && nj < E) {
					if (map[ni][nj] == 1 && chk[ni][nj] == false) {
						que.add(new pair1(ni, nj));
						map2[ni][nj] = cnt;
						cnt++;
					}
				}
			}

		}
	}

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		V = Integer.parseInt(sc.next());
		E = Integer.parseInt(sc.next());
		vmap = new char[V][E];
		map = new int[V][E];
		map2 = new int[V][E];
		chk = new boolean[V][E];
		que = new LinkedList<pair1>();
		cnt = 1;
		// 데이터 받아 오기
		for (int i = 0; i < V; i++) {
			vmap[i] = sc.next().toCharArray();
		}
		for (int i = 0; i < V; i++) {
			for (int j = 0; j < E; j++) {
				map[i][j] = vmap[i][j] - '0';
			}
		}
		bfs(0, 0);
		System.out.println(map2[V-1][E-1]);
		for (int i = 0; i < V; i++) {
			for (int j = 0; j < E; j++) {

			}
			System.out.println(Arrays.toString(map[i]));
		}
	}
}
