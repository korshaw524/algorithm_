/*
 * 아니 ... 아이디어는 쉽다 ..
 * 재귀에서 void로 하면 cnt가  메인로 왔을때 증가하지 않는다.
 * boolean 타입은 재귀에서 값이 변경되어도 연동이 되는거 같다.
 * 
 */

import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class Main_1012_유기농배추 {
	public static int m;
	public static int n;
	public static int cnt;
	public static int[][] map;
	public static int[] dn = { -1, 1, 0, 0 };
	public static int[] dm = { 0, 0, -1, 1 };
	public static boolean[][] chk;

	public static void dfs(int i, int j) {

		chk[i][j] = true;
		for (int a = 0; a < 4; a++) {
			int nn = i + dn[a];
			int nm = j + dm[a];
			if (nn >= 0 && nn < n && nm >= 0 && nm < m && map[nn][nm] == 1 && chk[nn][nm] == false) {
				dfs(nn, nm);
			}
		}

	}

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt(); // testcase 갯수
		for (int tc = 1; tc <= T; tc++) {
			cnt = 0;
			m = sc.nextInt(); // 가로 길이
			n = sc.nextInt(); // 세로 길이
			int s = sc.nextInt(); // 지렁이 갯수
			map = new int[n][m];
			chk = new boolean[n][m];

			// 맵 입력
			for (int a = 0; a < s; a++) {
				int x = sc.nextInt();
				int y = sc.nextInt();
				map[y][x] = 1;
			}
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					if (map[i][j] == 1 && chk[i][j] == false) {
						dfs(i, j);
						cnt++;
					}
				}
			}
			System.out.println(cnt);
		}
	}
}
