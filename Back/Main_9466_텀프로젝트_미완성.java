/*
 * 
 * 첫 아이디어 ..  재귀에 들어가기 전에 큐에 i값을 저장하고 재귀를 돌다가 큐의 값이랑 똑같으면 사이클이 구성된다고 판단
 * 처음 숫자인지를 확인을 못하네 ㅄ이네
 * 
 * 
 * 
 */



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.StringTokenizer;

public class Main_9466_텀프로젝트_미완성 {

	public static int n;
	public static int[][] map;
	public static boolean[][] chk;
	public static Stack<Integer> stk;

	public static int dfs(int i, int j) {
		chk[i][j] = true;
		
		if(!stk.isEmpty()) {
			int q = stk.pop();
			if(q == j || i==j ) {
				stk.clear();
				return 0;
			}
			
			else {
				for (int a = 1; a <= n; a++) {
					if (chk[j][a] == false && map[j][a] == 1) {
						return dfs(j,a);
					}
				}
			}
		}
		
		

		return 1;
		

	}

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());//1

		for (int tc = 1; tc <= T; tc++) {
			n = Integer.parseInt(br.readLine());//7
			StringTokenizer st = new StringTokenizer(br.readLine());//
			map = new int[n + 1][n + 1];
			chk = new boolean[n + 1][n + 1];
			stk = new Stack<>();
			
			for (int i = 1; i <=n; i++) {
				int v = Integer.parseInt(st.nextToken());
				map[i][v] = 1;
				continue;
			}


			int cnt = 0;
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++) {
					if (map[i][j] == 1 && chk[i][j] == false) {
						stk.push(i);
						cnt+=dfs(i, j);
						continue;
					}
				}
			}
			System.out.println(cnt);
		}
	}

}
