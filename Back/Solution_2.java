import java.util.Scanner;

public class Solution_2 {
	static String money;
	static char[] mon;
	static int len, ans;

	public static char[] swap(String mmoney, int i, int j ) {//3 2
		char ch[] = mmoney.toCharArray();
        char temp = ch[i];
        ch[i] = ch[j];
        ch[j] = temp;
        return ch;
		
		
	}

	public static void dfs(int num, int idx) {
		if (num == len) {
			ans = Math.max(ans, Integer.parseInt(money));
			return;
		}
		for (int i = idx; i < money.length(); i++) {
			for (int j = i + 1; j < money.length(); j++) {
				if (money.charAt(i) <= money.charAt(j)) {
					money = new String(swap(money,i,j));
					dfs(num + 1, i);
					money = new String(swap(money,j,i));
				}
			}
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int tc = 1; tc <= T; tc++) {
			money = sc.next();
			int num = sc.nextInt();
			mon = money.toCharArray();
			len = sc.nextInt();
			ans = 0;
			dfs(0, 0);
			System.out.println("#" + tc + " " + ans);
		}
	}

}
