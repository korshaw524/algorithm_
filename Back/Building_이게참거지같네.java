import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;


public class Building_이게참거지같네 {
	public static int[] di = { -1, -1, -1, 0, 1, 1, 1, 0 };
	public static int[] dj = { -1, 0, 1, 1, 1, 0, -1, -1 };
	public static boolean[][] chk;
	static Queue<Integer> list;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = Integer.parseInt(sc.next());
		String[][] a = new String[n][n];
		chk = new boolean[n][n];
		list = new LinkedList<Integer>();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				a[i][j] = sc.next();
				
				
			}
//			System.out.println(Arrays.toString(a[i]));
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (a[i][j].contains("B"))
					for (int k = 0; k < 8; k++) {
						int ni = i + di[k];
						int nj = j + dj[k];
						if (ni >= 0 && nj >= 0 && ni < n && nj < n && a[ni][nj].contains("G")) {
							chk[i][j] = true;
						}
					}
				if(chk[i][j] == false && a[i][j].contains("B")) {
//					System.out.println(i +" "+j);
					list.add(i);
					list.add(j);
				}
			}
		}
		
		int max =0;
		while(!list.isEmpty()) {
			int idxi = list.poll();
			int idxj = list.poll();
			int cnt = 0;
			
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					if(idxi == i || idxj == j) {
						if(a[i][j].contains("B")) {
							cnt++;
						}
					}
				}
//			System.out.println(Arrays.toString(a[i]));
			}
			if(max < cnt) {
				max = cnt;
			}
		}
		System.out.println(max);
		
	}

}

/*
3
G G B
G B B
B B B
5
G B G G B
G B G G B
B B B B G
B G B B B
G B B B B
6
G B G G B B
G B G G B G
B B B B G B
B G B B B B
G B B B B G
G B B B B G
 * 
 */
