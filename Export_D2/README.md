##알고리즘 공부

[SW Expert Academy](https://swexpertacademy.com)

[백준](https://www.acmicpc.net/)


## Tip
- BufferedReader
	- 1983. 조교의 성적 매기기
		- 실행시간이 빠른 사람의 코드를 보면 대부분 BufferedReader와 함께 StringTokenizer을 이용함
		- BufferedReader와 StringTokenizer 사용하기
		```
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer stk = new StringTokenizer(br.readLine());
		int n = Integer.parseInt(stk.nextToken()); 
		```
		- StringTokenizer는 읽어들인 데이터에서 공백을 기준으로 문자열을 토큰으로 처리함 , ```stk.nextToken()``` 을 사용해서 다음 입력값을 불러올 수 있음
		- Arrays.sort() 의 compare 함수 변경하기 => [Comparable](https://cwondev.tistory.com/15) 참고!!
		```
		@Override
		public int compareTo(Student o) {
			return score - o.score;
		}
		```
