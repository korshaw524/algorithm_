package D2;
import java.util.Scanner;

public class Solution_D2_2001_파리퇴치 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int tc = 1; tc <= T; tc++) {

			int n = sc.nextInt(); // 배열 크기
			int[][] num = new int[n][n];
			int m = sc.nextInt(); // 범위
			int max = 0;
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					num[i][j] = sc.nextInt();
				}
			}
			//배열 저장
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					int sum = 0;
					if (i + m <= n && j + m <= n) {
						for (int a = 0; a < m; a++) {
							for (int b = 0; b < m; b++) {
								sum+=num[i+a][j+b];
							}
						}
					}
					if(max<sum) {
						max=sum;
					}
				}
			}
			System.out.println("#"+tc+" "+max);
		}
	}

}
