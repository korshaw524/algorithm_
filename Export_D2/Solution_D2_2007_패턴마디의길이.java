package D2;
import java.io.FileInputStream;
import java.util.Scanner;

public class Solution_D2_2007_패턴마디의길이 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int tc = 1; tc <= T; tc++) {
			String line = sc.next();
			String s = "";
			int result = 0;
			
			k: for (int i = 0; i < line.length(); i++) {
				s += line.charAt(i);// 입력받아서 
				for (int j = 0; j < s.length(); j++) {
					String s1 = line.substring(0, s.length());//비교하자
					if (s1.equals(line.substring(s.length(), s.length()*2))) {
						result = s.length();//같으면 길이가 얼마
						break k;
					}
//				System.out.println(s1);
			}
			}
//			System.out.println(s);
			System.out.println("#" + tc + " " + result);
		}
	}
}