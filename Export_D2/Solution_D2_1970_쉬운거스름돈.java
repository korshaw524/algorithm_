package D2;
import java.util.Scanner;

public class Solution_D2_1970_쉬운거스름돈 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int tc = 1; tc <= T; tc++) {
			System.out.println("#"+tc);
			int in = sc.nextInt();
			int[] m = { 50000, 10000, 5000, 1000, 500, 100, 50, 10 };

			for (int i = 0; i < m.length; i++) {
				int cnt = 0;
				if(in / m[i] > 0) {
					cnt = in/m[i];
					in = in % m[i];
				}
				
				System.out.print(cnt+" ");
			}
			System.out.println();

		}
	}

}
