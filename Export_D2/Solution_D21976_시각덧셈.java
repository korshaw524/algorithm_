package D2;
import java.util.Scanner;

public class Solution_D21976_시각덧셈 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();

		for (int tc = 1; tc <= T; tc++) {
			int hour1 = sc.nextInt();
			int min1 = sc.nextInt();
			int hour2 = sc.nextInt();
			int min2 = sc.nextInt();
			int hs = hour1+hour2;
			int ms = min1+min2;
			if (hs > 12)
				hs = hs - 12;
			if (ms > 60) {
				hs = hs + 1;
				ms = ms - 60;
			}
			System.out.println("#"+tc+" "+hs+" "+ms);
		}
	}

}
