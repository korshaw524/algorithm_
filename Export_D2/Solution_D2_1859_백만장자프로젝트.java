package D2;

import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1859_백만장자프로젝트 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();

		for (int tc = 1; tc <= T; tc++) {
			long buy = 0;
			long cnt = 0;
			long mx = 0;
			int idx = 0;
			int n = sc.nextInt();
			long total = 0;
			long[] revenue=new long[n];
			long[] cost = new long[n];

			for (int i = 0; i < n; i++) {
				cost[i] = sc.nextInt();
				if (mx <= cost[i]) {
					mx = cost[i]; // 최대 가격
					idx = i; // 최대 가격 위치
				}
			}
			for (int i = 0; i < n; i++) {
				if(i<idx){
					buy+=cost[i];
					cnt++;
				}
				else if(i==idx){
					revenue[i] =(cost[i]*cnt)-buy;
					total+=revenue[idx];
				}
				else{
					mx=0;
					cnt=0;
					buy=0;
//					total+=revenue[idx];
//					System.out.println(total);
					for(int j=idx+1;j<n;j++){
						if(mx <=cost[j]){
							mx = cost[j];
							idx=j;
						}
					}
					if(i<idx){
						buy+=cost[i];
						cnt++;
					}
					
				}
			}
			System.out.println("#"+tc+" "+total);
		}
	}

}
