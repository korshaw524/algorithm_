package D2;
import java.util.Scanner;
 
public class Solution_D2_1926_간단한369게임 {
 
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int N=sc.nextInt();
        int cnt,t,r;
        for(int i=1;i<=N;i++) {
            t=i;
            cnt=0;
            while(t!=0) {
                r=t%10;
                t/=10;
                if(r!=0 && r%3==0) cnt++;
            }
            if(cnt>0) {
                for(int k=0;k<cnt;k++) {
                    System.out.print('-');
                }
            }
            else {
                System.out.print(i);
            }
            System.out.print(" ");
        }
 
    }//end main
 
}