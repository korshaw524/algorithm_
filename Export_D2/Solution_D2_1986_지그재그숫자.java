package D2;
import java.util.Scanner;
public class Solution_D2_1986_지그재그숫자 {
    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
       int T = sc.nextInt();
       
       for(int tc=1;tc<=T;tc++){
    	   int n = sc.nextInt();
    	   int sum =0;
    	   for(int i = 1; i<=n;i++){
    		   if(i%2==0){
    			  sum-=i;
    		   }
    		   else {
    			   sum+=i;
    		   }
    		   
    	   }
    	   System.out.println("#"+tc+" "+sum);
       }
    }
}