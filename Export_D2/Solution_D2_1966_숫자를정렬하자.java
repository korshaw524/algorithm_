package D2;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D2_1966_숫자를정렬하자 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int tc = 1; tc <= T; tc++) {
			System.out.print("#"+tc+" ");
			int n = sc.nextInt();
			int[] in = new int[n];
			for(int j = 0; j<n;j++) {
				in[j] = sc.nextInt();
			}
			Arrays.sort(in);
			
			for(int i = 0 ; i<n;i++) {
				System.out.print(in[i] +" ");
			}
			System.out.println();
		}

	}

}
