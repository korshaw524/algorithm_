package D2;
import java.util.Scanner;

public class Solution_D2_1204_최빈수구하기 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int a = 1; a <= T; a++) {
			int tc = sc.nextInt();
			int[] cnt = new int[101];
			for (int i = 0; i < 1000; i++) {
				int num = sc.nextInt();
				cnt[num]++;
			}
			int max = 0;
			int ans =0;
			for (int i = 1; i < 101; i++) {
				if (max <= cnt[i]) {
					max = cnt[i];
					ans = i;
				}
			}
			System.out.println("#"+tc+" "+ans);

		}

	}

}
