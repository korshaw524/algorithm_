package com.d1;

import java.util.Scanner;

public class Solution_d1_2072_홀수만더하기 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc=1;tc<=T;tc++){
			int sum=0;
			for(int a=0;a<10;a++){
				int input = sc.nextInt();
				if(input%2!=0){
					sum+=input;
				}
			}System.out.println("#"+tc+" " +sum);
		}sc.close();
	}

}
