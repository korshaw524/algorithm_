package D3;
	import java.util.ArrayList;
	import java.util.Scanner;
	
	public class Solution_D3_7510_상원이의연속합 {
	
		public static void main(String[] args) {
			Scanner sc = new Scanner(System.in);
			int T = Integer.parseInt(sc.nextLine());
			for(int tc =1; tc<=T;tc++) {
				int s = sc.nextInt();
				int sum =0;
				int cnt=1;
				if(s==1) {
					System.out.println("#"+tc+" "+s);
				}
				else {
					for(int i =1; i<=s;i++) {
						sum=i;
						for(int j=i+1;j<=s;j++) {
							
							if(sum==s) {
								cnt++;
								break;
							}
							else if(sum>s) {
								break;	
							}
							else {
								sum+=j;
							}
						}
					}
					System.out.println("#"+tc+" "+cnt);
					
				}
				
			}
			
		}
	
	}
