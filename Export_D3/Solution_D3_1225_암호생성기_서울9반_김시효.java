package D3;


import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Solution_D3_1225_암호생성기_서울9반_김시효 {

	public static void main(String[] args) throws Exception {

		Scanner sc = new Scanner(System.in);

		for (int tc = 0; tc < 10; tc++) {
			int t = sc.nextInt();
			int[] n = new int[8];
			Queue<Integer> q = new LinkedList<Integer>();
			int front = 0;
			int rear = 0;
			for (int i = 0; i < 8; i++) {
				q.offer(sc.nextInt());
			}
			int i = 1;
			while (q.peek() - i > 0) {
				int temp = q.poll() - i;
				q.offer(temp);

				if (i == 5)
					i = 1;
				else
					i++;

			}
			int temp1 = q.poll();
			q.offer(0);

			System.out.print("#" + t + " ");
			for (int j = 0; j < 8; j++) {
				System.out.print(q.poll() + " ");

			}
			System.out.println();

			
		}

	}

}