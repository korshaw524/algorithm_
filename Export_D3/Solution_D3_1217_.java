package D3;
import java.util.Scanner;

public class Solution_D3_1217_ {
	public static int ans;
	public static int re(int num, int cnt) {
		
		if(cnt>=1) {
	//		System.out.println(ans);
			ans = num * re(num,cnt-1);
			return ans;
		}
		else {
			return 1;
		}
		
		
	}
	
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		for(int tc = 1; tc <= 10; tc++) {
			int T = sc.nextInt();
			int num =sc.nextInt();
			int cnt = sc.nextInt();
			System.out.println("#"+tc+" "+ re(num ,cnt));
		}
	}

}
