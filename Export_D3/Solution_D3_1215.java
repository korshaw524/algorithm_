package D3;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D3_1215 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		for (int tc = 1; tc <= 10; tc++) {
			int n = Integer.parseInt(sc.nextLine());
			char[][] c = new char[8][8];
			int cnt = 0;
			for (int i = 0; i < 8; i++) {
				String s = sc.nextLine();
				for (int j = 0; j < 8; j++) {
					char cut = s.charAt(j);
					c[i][j] = cut;
				}
			}
			// 3일때 예외 추가하자
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j <= 8 - n; j++) {
					if (n == 4 || n == 5) {
						if (c[i][j] == c[i][j + n - 1] && c[i][j + 1] == c[i][j + n - 2]) {
							cnt++;
						}
					} else if (n == 3) {
						if (c[i][j] == c[i][j + n - 1]) {
							cnt++;
						}
					} else {
						if (c[i][j] == c[i][j + n - 1] && c[i][j + 1] == c[i][j + n - 2]
								&& c[i][j + 2] == c[i][j + n - 3]) {
							cnt++;
						}
					}

				}
			}
			for (int i = 0; i <= 8 - n; i++) {
				for (int j = 0; j < 8; j++) {
					if (n == 4 || n == 5) {
						if (c[i][j] == c[i + n - 1][j] && c[i + 1][j] == c[i + n - 2][j]) {
							cnt++;
						}
					} else if (n == 3) {
						if (c[i][j] == c[i + n - 1][j]) {
							cnt++;
						}
					} else {
						if (c[i][j] == c[i + n - 1][j] && c[i + 1][j] == c[i + n - 2][j]
								&& c[i + 2][j] == c[i + n - 3][j]) {
							cnt++;
						}
					}
				}

			}
			System.out.println("#" + tc + " " + cnt);
		}
	}
}
