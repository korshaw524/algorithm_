import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
 
public class Solution_D3_1209_Sum {
 
    public static void main(String[] args) throws FileNotFoundException  {
    	System.setIn(new FileInputStream("res/input_Sum.txt"));
        Scanner sc = new Scanner(System.in);
         
        for(int tc=1; tc<=10; tc++) {
            int t=sc.nextInt();
            int N=100;
            int[][] a = new int[N][N];
            for(int i=0; i<N;i++) {
                for(int j=0; j<N;j++) {
                    a[i][j] = sc.nextInt();
                    }
            }
            int max=-1;
            for(int i=0; i<N;i++) {
                int sum =0;
                for(int j=0; j<N;j++) {
                    sum+=a[i][j];
                     
                }
                if(max<sum) max=sum;
                 
            }
            for(int j=0; j<N;j++){
                int sum =0;
                for(int i=0; i<N;i++) {
                    sum+=a[i][j];
                     
                }
                if(max<sum) max=sum;
                 
            }
             
            for(int i=0;i<N;i++) {
                int sum=0;
                for(int j=0;j<N;j++) {
                    if(i==j) sum+=a[i][j];
                }
                if(max<sum) max=sum;
            }
             
            for(int i=0;i<N;i++) {
                int sum=0;
                for(int j=0;j<N;j++) {
                    if((i+j)==(N-1)) sum+=a[i][j];
                     
                }
                if(max<sum) max=sum;
            }
             
            System.out.println("#"+tc+" "+max);
        }
         
    }
}
