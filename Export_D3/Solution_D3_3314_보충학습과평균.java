package D3;
import java.util.Scanner;

public class Solution_D3_3314_보충학습과평균 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();

		for (int tc = 1; tc <= T; tc++) {
			int[] num = new int[5];
			int sum = 0, ans = 0;
			for (int per = 0; per < 5; per++) {// 5명
				num[per] = sc.nextInt();
				if (num[per] < 40) {
					num[per] = 40;
				}
				sum += num[per];
			}
			ans = sum / 5;
			System.out.println("#"+tc+" "+ans);
		}
	}

}
