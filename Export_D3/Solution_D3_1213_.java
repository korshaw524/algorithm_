package D3;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_1213_ {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		for (int tc = 1; tc <= 1; tc++) {
			int T = sc.nextInt();
			int[] a = new int[T];
			// 건물 높이 입력 받음
			for (int i = 0; i < T; i++) {
				a[i] = sc.nextInt();
			}
			//
			int sum = 0;
			
			for (int i = 2; i < T - 2; i++) {
				int min = 256;
				if (a[i] >= (a[i - 1] + 2) && a[i] > a[i - 2] && a[i] >= (a[i + 1] + 2) && a[i] > a[i + 2]) {
					if (a[i] - a[i - 2] == 1 || a[i] - a[i + 2] == 1) {
						sum += 1;
					} else {
						int aa = a[i] - a[i - 1];
						int bb = a[i] - a[i - 2];
						int cc = a[i] - a[i + 2];
						int dd = a[i] - a[i + 1];
						if (min > aa)
							min = aa;
						if (min > bb)
							min = bb;
						if (min > cc)
							min = cc;
						if (min > dd)
							min = dd;

						sum+=min;
					}
				}
			}
			System.out.println("#"+tc+" "+sum);

		}

	}

}
