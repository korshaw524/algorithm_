package D3;
import java.util.Scanner;
 
public class Solution_D3_1232_사칙연산유효성검사 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        for (int tc = 1; tc <= 10; tc++) {
            int cnt = 0;
            int cnt2 = 0;
            int p = 0;
            int N = sc.nextInt();
            sc.nextLine();
            for (int i = 0; i < N; i++) {
            String[] T = sc.nextLine().split(" ", 2);
                if (Character.isDigit(T[1].charAt(0)))
                    cnt++;
                else
                    cnt2++;
            }
            if (cnt2+1 == cnt)
                p = 1;
            System.out.println("#" + tc + " " + p);
        }
    }
}
