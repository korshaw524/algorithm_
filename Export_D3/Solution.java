package D3;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;
 
public class Solution{
 
    public static void main(String[] args) throws Exception{
        System.setIn(new FileInputStream("res/input_d4_1210.txt"));
        Scanner sc = new Scanner(System.in);
 
        for(int tc=0;tc<10;tc++) {
            int T =sc.nextInt();
            System.out.print("#"+T+" ");
            
            int[][] arr = new int[100][100];
            for(int i=0;i<100;i++){
                for(int j=0;j<100;j++) {
                    arr[i][j]=sc.nextInt();
                }
            }
            int x=99;
            int y=0;
            
            for(int i=0;i<100;i++) {
            	if(arr[99][i]==2) y=i;
            }
            
            //System.out.println(y);
            while(true) {
                if(x==0) {
                    System.out.println(y);
                    break;
                }
                if(y>=1 && arr[x][y-1] == 1) {
                    while(y<100 && y!=0 && arr[x][y-1]!=0) {
                        y--;
                    }
                    x--;
                }
                else if( y<=98 && arr[x][y+1]==1) {
                    while(y!=99&&arr[x][y+1]!=0) {
                        y++;
                    }
                    x--;
                }
                else x--;
                
            }
        }
    }
 
}