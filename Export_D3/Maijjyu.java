package D3;
import java.util.LinkedList;
import java.util.Queue;

public class Maijjyu {

	public static void main(String[] args) {
		Queue<int[]> q = new LinkedList<int[]>();
		int num =1;
		int cnt =20;// 개수
		int tot =0; 
		int[] recv=null;//주는
		q.offer(new int[] {num,1});
		while(cnt>0) {
			recv = q.poll();
			int su = (recv[1]>cnt)? cnt:recv[1];
			cnt -=su;
			tot += su;
			System.out.println(recv[0]+"번"+su+"개 남은 수: "+cnt);
			recv[1] = recv[1]+1;
			q.offer(recv);
			q.offer(new int[] {++num,1});
		}
		System.out.println("마지막 받은 사람은 "+recv[0]);
	}

}
