package D3;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_4406_모음이보이지않는사람 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = Integer.parseInt(sc.nextLine());

		for (int tc = 1; tc <= T; tc++) {
			ArrayList<Character> list = new ArrayList<>();
			String s = sc.nextLine(); // fluid
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);
				if (c != 'a' && c != 'e' && c != 'o' && c != 'u' && c != 'i') {
					list.add(c);
				}
			}
//			System.out.print(list.size());
//			System.out.println(list);
			System.out.print("#"+tc+" ");
			for (int i = 0; i < list.size(); i++) {
				
				System.out.print(list.get(i));
			}
			System.out.println();
		}
	}
}
