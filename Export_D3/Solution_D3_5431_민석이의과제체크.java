package D3;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
 
//workshop;
public class Solution_D3_5431_민석이의과제체크{
 
 
        public static void main(String[] args) throws FileNotFoundException  {
        	System.setIn(new FileInputStream("res/input_민석이의과제체크.txt"));
            Scanner sc = new Scanner(System.in);
            int t = sc.nextInt(); 
            for (int i = 0; i < t; i++) { 
                int num = sc.nextInt();
                int cnt = sc.nextInt(); 
                int [] arr = new int [cnt];
                int [] list = new int [num];
                  
                for (int j = 0; j <num ; j++) { 
                    list[j] = j+1;
                }
                  
                for (int j = 0; j <cnt ; j++) {
                    arr[j] = sc.nextInt();
                }
                  
                Arrays.sort(arr);
                  
                for (int j = 0; j < num; j++) {
                    for (int k = 0; k < arr.length; k++) {
                        if(list[j] == arr[k]) {
                            list[j] = 0;
                        }
                    }
                }
                System.out.print("#"+(i+1)+" ");
                for (int j = 0; j < list.length; j++) {
                    if(list[j] != 0) System.out.print(list[j]+" ");
                } System.out.println();
            }
        }
    }