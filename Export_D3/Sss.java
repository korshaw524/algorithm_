package D3;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;
  
public class Sss {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//      br = new BufferedReader(new StringReader(src));
  
        for (int index = 1; index <= 10; index++) {
            int n1 = Integer.parseInt(br.readLine()); //암호문 길이
            LinkedList<String> list = new LinkedList<String>();
            LinkedList<String> list2 = new LinkedList<String>();
            String line = br.readLine();
            StringTokenizer token = new StringTokenizer(line);
            while (token.hasMoreTokens()) {
                list.add(token.nextToken()); // 암호문
            }
            int n2 = Integer.parseInt(br.readLine());//명령어 개수
            String line2 = br.readLine();
            token = new StringTokenizer(line2);
            while (token.hasMoreTokens()) {
                list2.add(token.nextToken()); //명령어 받음
            }
  
            while (!list2.isEmpty()) {
                String alpha = list2.poll();
                if (alpha.equals("I")) {
                    int idx = Integer.parseInt(list2.poll());
                    int count = Integer.parseInt(list2.poll());
                    for (int i = 0; i < count; i++) {
                        list.add(idx, list2.poll());
                        idx++;
                    }
                }
                else if (alpha.equals("D")) {
                    int idx = Integer.parseInt(list2.poll());
                    int count = Integer.parseInt(list2.poll());
                    for(int i=0; i<count; i++) {
                        list.remove(idx);
                    }
                }
                else if (alpha.equals("A")) {
                    int count = Integer.parseInt(list2.poll());
                    for(int i=0; i<count; i++) {
                        list.add(list2.poll());
                    }
                }
  
            }
            System.out.print("#" + index + " ");
            for (int i = 0; i < 10; i++)
                System.out.print(list.poll() + " ");
            System.out.println();
        }
    }
}