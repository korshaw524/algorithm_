package D3;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution_D3_1213 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		for (int tc = 1; tc <= 1; tc++) {
			int T = sc.nextInt();
			int[] a = new int[T];
			// 건물 높이 입력 받음
			for (int i = 0; i < T; i++) {
				a[i] = sc.nextInt();
			}
			//
			int dr1 = 0;// 1칸차이 건물
			int dr2 = 0;// 2칸 차이 건물
			int dl1 = 0;
			int dl2 = 0;
			int sum = 0;

			for (int i = 2; i < T - 2; i++) {
				int min = 256;
				dl1 = a[i] - a[i - 1]; // 왼쪽 한칸 높이차이
				dl2 = a[i] - a[i - 2]; // 왼쪽 두칸 높이 차이
				dr1 = a[i] - a[i + 1]; // 오른쪽 한칸 높이 차이
				dr2 = a[i] - a[i + 2]; // 오른쪽 두칸 높이 차이
				if (dl1 >= 2 && dl2 >= 1 && dr1 >= 2 && dr2 >= 1) {
					if (dl2 == 1 || dr2 == 1) {
						sum += 1;
					} else {
						if(min > dr1) min = dr1;
						if(min > dr2) min = dr2;
						if(min > dl1) min = dl1;
						if(min > dl2) min = dl2;
						
						sum+=min;

					}

				}
			}
			System.out.println("#"+tc+" "+sum);
		}

	}

}
