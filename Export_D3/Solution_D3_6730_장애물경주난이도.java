package D3;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D3_6730_장애물경주난이도 {

	public static void main(String[] args) throws Exception {

		Scanner sc = new Scanner(System.in);
		int T = Integer.parseInt(sc.nextLine());// 테스트케이스 갯수
		for (int tc = 1; tc <= T; tc++) {
			int num =Integer.parseInt(sc.next()); // 장애물 갯수
			int[] sv = new int[num];
			int max = 0;
			int mmax =0;
			for (int i = 0; i < num; i++) {
				
				int obs = sc.nextInt(); // 장애물 높이
				sv[i] = obs;
				
			}
			
			for(int i =0; i<num;i++) {
				int h = 0;
				if(i+1 < num)
					h = sv[i+1] - sv[i];
//				System.out.println(h);
				
				if(h>0) {
					if(max<h) {
						max=h;
					}
				}
				else if(h<0) {
					if(mmax>h) {
						mmax=h;
					}
				}
			}
			System.out.println("#"+tc+" "+max+" "+Math.abs(mmax));
		}
	}
}
