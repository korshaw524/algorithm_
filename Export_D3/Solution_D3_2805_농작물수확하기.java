package D3;
import java.util.Scanner;

public class Solution_D3_2805_농작물수확하기 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = Integer.parseInt(sc.nextLine());
		for (int tc = 1; tc <= T; tc++) {

			int n = Integer.parseInt(sc.nextLine()); // 배열의 크기
			char[][] arr = new char[n][n];
			int total = 0;
			int sum = 0;
			int cc = 0;
			
			for (int i = 0; i < n; i++) { // 배열 받기
				String input = sc.nextLine();
				for (int j = 0; j < n; j++) {
					char[] c = new char[n];
					c[j] = input.charAt(j);
					arr[i][j] = c[j];
					cc = c[j] - '0';
					total += cc;
				}
			}
			
	
//			System.out.println(total);

			int m = 0;
			int ct = n / 2;
			for (int i = 0; i < n; i++) {

				for (int j = 0; j < n; j++) {

					if (i < ct && j < ct - m) {// ct=3
						cc = arr[i][j] - '0';
						sum += cc;

					} else if (i < ct && j > ct + m) {// 2
						cc = arr[i][j] - '0';
						sum += cc;

					} else if (i > ct && j < ct - m) {// 3
						cc = arr[i][j] - '0';
						sum += cc;

					} else if (i > ct && j > ct + m) {// 4
						cc = arr[i][j] - '0';
						sum += cc;
					}

				}
				if (i < ct) {
					m++;
				} else if (i > ct) {
					m--;
				} else {
					m = m - 1;
				}
			}
//			System.out.println(sum);
			System.out.print("#" + tc + " ");
			System.out.println(total - sum);

		}
	}

}
