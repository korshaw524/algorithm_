package D3;
import java.util.Scanner;

public class Solution_D3_1240_단순2진암호코드 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();

		for (int tc = 1; tc <= T; tc++) {
			String[] s = new String[10];
			s[0] = "0001101";
			s[1] = "0011001";
			s[2] = "0010011";
			s[3] = "0111101";
			s[4] = "0100011";
			s[5] = "0110001";
			s[6] = "0101111";
			s[7] = "0111011";
			s[8] = "0110111";
			s[9] = "0001011";

			int N = sc.nextInt(); // i
			int M = sc.nextInt(); // j
			String[] sn = new String[N];
			int idx1 = 0, idx2 = 0;
			for (int i = 0; i < N; i++) {
				sn[i] = sc.next();
				for (int j = M - 1; j > 0; j--) {
					if (sn[i].contains("1") && sn[i].charAt(j) == '1') {
						idx1 = i;
						idx2 = j;
						break;
					}
				}
			}

			String ss = sn[idx1].substring(idx2 - 55, idx2 + 1);
			int cnt = 0;
			int cnt1 = 0;
			int cnt2 = 0;
			int sum = 0;
			int[] num = new int[8];
			for (int i = 0; i < 56; i = i + 7)
				for (int j = 0; j < 10; j++)
					if (ss.substring(i, i + 7).equals(s[j]))
						num[cnt++] = j;

			for (int i = 0; i < 8; i++) {
				if (i % 2 == 0) {
					cnt1 += num[i];
				} else {
					cnt2 += num[i];
					sum += num[i];
				}
			}
			sum = cnt1 * 3 + cnt2;
			if (sum % 10 != 0) {
				sum = 0;
			} else {
				sum = cnt1 + cnt2;
			}
			System.out.println("#" + tc + " " + sum);

		}
	}
}
