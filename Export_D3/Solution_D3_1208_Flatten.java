import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
//workshop;

public class Solution_D3_1208_Flatten {
	    public static void main(String[] args) throws Exception {
	    	System.setIn(new FileInputStream("res/input_Flatten.txt"));
	    	Scanner sc = new Scanner(System.in);
	    	 int cnt=0;
	            for(int b=1;b<=10;b++) {
	                int T = sc.nextInt();
	                int[] a = new int[100];
	                    for(int c = 0; c<100;c++) {
	                        int input = sc.nextInt();
	                        a[c] =input;
	                    }
	                    for(int j=0;j<=T;j++) {
	                        int max = -100;
	                        int min = 100;
	                        int cnt1 = 0;
	                        int cnt2 = 0;
	                        for(int i=0;i<100;i++) {
	                            if(a[i]>max) {
	                                max=a[i];// 최대
	                                cnt1=i; // 최대 위치
	                            }
	                            if(a[i]<min) {
	                                min=a[i];   //최소
	                                cnt2=i;     // 최소 위치
	                            }
	                        }
	                    a[cnt1]=a[cnt1]-1;
	                    a[cnt2]=a[cnt2]+1;
	                    cnt = max-min;
	                    }
	               System.out.println("#"+b+" "+cnt);
	            }
	        }
	    }