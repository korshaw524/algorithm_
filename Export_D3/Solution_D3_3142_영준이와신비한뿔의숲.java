package D3;
import java.util.Scanner;

public class Solution_D3_3142_영준이와신비한뿔의숲 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int T = sc.nextInt();
		for(int tc = 1; tc <= T; tc++){
			int n = sc.nextInt(); // 뿔의 수
			int m = sc.nextInt(); // m마리
			
			int twin = n-m; //2 
			int uni =m-twin; //1
			
		    System.out.println("#"+tc+" "+uni+ " "+twin);
			
		}
	}

}
