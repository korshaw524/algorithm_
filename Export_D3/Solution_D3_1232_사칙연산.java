package D3;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;
 
class Node {
    String data;
    int left;
    int right;
 
    Node(String data) {
        this.data = data;
        this.left = 0;
        this.right = 0;
    }
 
    Node(String data, int left) {
        this.data = data;
        this.left = left;
        this.right = 0;
    }
 
    Node(String data, int left, int righ) {
        this.data = data;
        this.left = left;
        this.right = righ;
    }
}
 
public class Solution_D3_1232_사칙연산 {
    public static ArrayList<Node> node;
    public static Stack<Integer> stack = new Stack<>();
    public static int sum, temp;
     
     public static void postorder(int n) {
            if (n != 0) {
                postorder(node.get(n - 1).left);
                postorder(node.get(n - 1).right);
                cal(node.get(n - 1).data);
            }
        }
      
        public static void cal(String c) {
            if(c.equals("+")||c.equals("-")||c.equals("*")||c.equals("/")) {
                int n2 = stack.pop();
                int n1 = stack.pop();
                int nn = 0;
               
                switch(c.charAt(0)) {
                    case '+': nn= n1+n2; break;
                    case '-': nn= n1-n2; break;
                    case '*': nn= n1*n2; break;
                    case '/': nn= n1/n2; break;
                }
                stack.push(nn);
            }else{
                stack.push(Integer.parseInt(c));
            }
        }
     
    public static void main(String[] args) throws Exception{
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            for (int tc = 1; tc <= 10; tc++) {
                int N = Integer.parseInt(br.readLine());
                node =  new ArrayList<>();
                for (int i = 0; i < N; i++) {
                    sum = 0;
                    temp = 0;
                    StringTokenizer st = new StringTokenizer(br.readLine());
                    st.nextToken();
                    if (st.countTokens() == 1) {
                        node.add(new Node(st.nextToken()));
                    } else if (st.countTokens() == 2) {
                        node.add(new Node(st.nextToken(), Integer.parseInt(st.nextToken())));
                    } else {
                        node.add(new Node(st.nextToken(), Integer.parseInt(st.nextToken()),
                                Integer.parseInt(st.nextToken())));
                    }
                }
             
            postorder(1);
            if(stack.isEmpty())
            System.out.println("#" + tc + " "+stack.pop());
        }
    }
 
}