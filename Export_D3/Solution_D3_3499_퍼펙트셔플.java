package D3;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D3_3499_퍼펙트셔플 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = Integer.parseInt(sc.next());
		for(int tc = 1; tc <=T; tc++) {
			int num = Integer.parseInt(sc.next());
			String[] arr = new String[num];
			for(int i = 0; i <num;i++) {
				String input = sc.next();
				arr[i] = input;
			}
			String[] arr2 = new String[num];
			//짝수일떄 
			if(num%2==0) {
				int k =0;
				System.out.print("#"+tc+" ");
				for(int i=0;i<num;i++) {
						if(i%2==0) {
							arr2[i]=arr[i-k];
							
						}
						else if(i%2==1){
							arr2[i] = arr[num/2+k];
							k=k+1;
					}
						System.out.print(arr2[i]+" ");
				}
			}
			
			if(num%2 ==1) {
				int k =0;
				System.out.print("#"+tc+" "+arr[0]+" ");
				for(int i=1;i<num;i++) {
						if(i%2==0) {
							arr2[i]=arr[i-k];
							
						}
						else if(i%2==1){
							arr2[i] = arr[num/2+k+1];
							k=k+1;
					}
						System.out.print(arr2[i]+" ");
				}
			}
			System.out.println();
		}
	}

}

