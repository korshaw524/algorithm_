package D3;

import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;


public class Solution_D3_7728_다양성측정 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = Integer.parseInt(sc.nextLine()); // 테스트 케이스
		for (int tc = 1; tc <= T; tc++) {
			char[] input;
			char[] cnt=new char[100];
			int a=0;
			input = sc.nextLine().toCharArray(); // array로 자르기 1512
			for(int i =0;i<input.length;i++) {
				cnt[input[i]]++;
				if(cnt[input[i]]==1) {
					a++;
				}
			}
			System.out.println("#"+tc+" "+a);
		}
	}
}