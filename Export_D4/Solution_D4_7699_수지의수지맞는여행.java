package D4;

import java.util.Scanner;

public class Solution_D4_7699_수지의수지맞는여행 {
	public static int[] di = { -1, 1, 0, 0 };
	public static int[] dj = { 0, 0, -1, 1 };
	public static boolean[] chk;
	public static char[][] a;
	public static int r, c, ans;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = Integer.parseInt(sc.next());

		for (int tc = 1; tc <= T; tc++) {
			r = Integer.parseInt(sc.next()); // 행
			c = Integer.parseInt(sc.next());// 열
			a = new char[r][c];
			chk = new boolean[27];
			for (int i = 0; i < r; i++) {
				String s = sc.next();
				a[i] = s.toCharArray();
			}
			ans = 0;
			dfs(0, 0, 1);
			System.out.println("#" + tc + " " + ans);
		}

	}

	public static void dfs(int i, int j, int cnt) {
		if (cnt == 27)
			return;
		if (ans < cnt)
			ans = cnt;
		chk[a[i][j] - 'A'] = true;

		for (int z = 0; z < 4; z++) {
			int ni = i + di[z];
			int nj = j + dj[z];
			if (ni < 0 || ni >= r || nj < 0 || nj >= c || chk[a[ni][nj] - 'A'])
				continue;
			dfs(ni, nj, cnt + 1);
		}
		chk[a[i][j] - 'A'] = false;
	}

}
