package D4;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;
 
public class Solution_D4_1218_괄호짝짓기_서울9반_김시효 {
    public static void main(String args[]) throws Exception {
         System.out.println(new FileInputStream("res/input_1218.txt"));
        Scanner sc = new Scanner(System.in);
        Stack<Character> stack=null;
         
        for(int tc=1; tc<=10; tc++) {
            stack=new Stack<Character>();
            int n =sc.nextInt();
            String s =sc.next();
             
            int answer=1;
             
            for(int i=0; i<s.length(); i++){
                char c=s.charAt(i);
                if(c=='(' || c=='[' || c=='{' || c=='<') {
                    stack.push(c);
                }else {
                    char ch=stack.pop();
                    if( (c==')' && ch!='(') ||
                        (c==']' && ch!='[') ||
                        (c=='}' && ch!='{') ||
                        (c=='>' && ch!='<'))  {
                        answer=0;
                        break;
                    }
                }
             
            }
            System.out.println("#"+tc+" " +answer);
        }
    }
}