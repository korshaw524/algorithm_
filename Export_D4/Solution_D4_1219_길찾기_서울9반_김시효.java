package D4;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Solution_D4_1219_길찾기_서울9반_김시효 {

    // public static int E;
    public static int[][] graph;
    public static boolean[] visit;
 
    public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("res/input_1219.txt"));
        Scanner sc = new Scanner(System.in);
        for (int tc = 1; tc <= 10; tc++) {
            graph = new int[100][100];
            visit = new boolean[100];
            int T = sc.nextInt();
            int E = sc.nextInt();
             
 
            for (int i = 0; i < E; i++) {
                int v1 = sc.nextInt();
                int v2 = sc.nextInt();
                graph[v1][v2] = 1;
            }
             
            dfsr(0);
            int ans = 0;
            if (visit[99])
                ans = 1;
            System.out.println("#" + tc + " " + ans);
 
        }
    }
 
    public static void dfsr(int node) {
        visit[node] = true;
        // System.out.print(node + " ");
        for (int next = 0; next < 100; next++) {
            if (visit[next] == false && graph[node][next] == 1)
                dfsr(next);
 
        }
 
    }
 
}
